import client_server.Database;
import client_server.Server;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;


class ServerTest {
    private final Database database = new Database();
    private final Server server = new Server();


    @BeforeEach
    void before1() throws SQLException {
        database.deleteAll();
        server.addUser("kamil", "pass");
    }


    // ----------------------UPTIME TESTS---------------------
    @Test
    void uptime() {
        Assertions.assertTrue(server.uptime().matches("Server is running for: ......."));
    }


    // ----------------------IS LOGGED USER TESTS---------------------
    @Test
    void userLogged() throws SQLException {
        server.login("kamil", "pass");
        boolean logged = server.checkLogin();

        Assertions.assertFalse(logged);
    }


    // ----------------------IS USER EXISTS TESTS---------------------
    @Test
    void isUserExists() throws SQLException {
        server.addUser("lukasz", "pass");

        Assertions.assertTrue(database.isUserExists("lukasz"));
        Assertions.assertTrue(database.isUserExists("kamil"));
    }


    // ----------------------ADDING USER TESTS---------------------
    @Test
    void addValidUser() throws Exception {
        Assertions.assertTrue(database.isUserExists("kamil"));
    }

    @Test
    void addEmptyUserTest() throws Exception {
        server.addUser("", "");

        Assertions.assertFalse(database.isUserExists(""));
    }


    // ----------------------LOGGING USER TESTS---------------------
    @Test
    void loginValidUser() throws SQLException {
        Assertions.assertTrue(server.login("kamil", "pass"));
    }

    @Test
    void loginInvalidUsername() throws SQLException {
        server.addUser("marcin", "pass");

        Assertions.assertFalse(server.login("kamilWrong", "pass"));
    }

    @Test
    void loginInvalidPassword() throws SQLException {
        Assertions.assertFalse(server.login("kamil", "wrongPass"));
    }


    // ----------------------SETTING ADMIN TESTS---------------------
    @Test
    void setAdminValidUserAndValidAdminPassword() throws SQLException {
        server.login("kamil", "pass");
        String response = server.setAdmin("kamil", "admin");

        Assertions.assertEquals("User: kamil privileges upgraded to admin", response);
    }

    @Test
    void setAdminInvalidUserAndValidAdminPassword() throws SQLException {
        server.login("kamil", "pass");
        String response = server.setAdmin("marcin", "admin");

        Assertions.assertEquals("User doesn't exists!", response);
    }

    @Test
    void setAdminValidUserAndInvalidAdminPassword() throws SQLException {
        server.login("kamil", "pass");
        String response = server.setAdmin("kamil", "WrongAdminPass");

        Assertions.assertEquals("Wrong admin password", response);
    }


    // ----------------------REMOVING USER TESTS---------------------
    @Test
    void removeValidUser() throws SQLException {
        server.addUser("mario", "bros");
        server.login("kamil", "pass");
        server.setAdmin("kamil", "admin");
        String response = server.removeUser("mario");

        Assertions.assertFalse(database.isUserExists("mario"));
        Assertions.assertEquals("User removed", response);
    }

    @Test
    void removeInvalidUser() throws SQLException {
        server.addUser("mario", "bros");
        server.login("kamil", "pass");
        server.setAdmin("kamil", "admin");
        String response = server.removeUser("marioWrong");

        Assertions.assertEquals("User doesn't exists!", response);
    }

    @Test
    void removeValidUserButNotAdmin() throws SQLException {
        server.addUser("mario", "bros");
        server.login("kamil", "pass");
        String response = server.removeUser("marioWrong");

        Assertions.assertFalse(database.isUserAdmin("kamil"));
        Assertions.assertEquals("You are not Admin!", response);
    }


    // ----------------------SENDING MESSAGE TESTS---------------------

    @Test
    void sendMessageToNotExistingUser() throws SQLException {
        server.addUser("mario", "bros");
        server.login("kamil", "pass");
        String response = server.sendMessage("marioWrong", "hej");

        Assertions.assertEquals("Wrong user!", response);
        Assertions.assertFalse(database.isUserExists("marioWrong"));
    }

    @Test
    void sendValidMessageToExistingUser() throws SQLException {
        server.addUser("mario", "bros");
        server.login("kamil", "pass");
        String response = server.sendMessage("mario", "hej");

        Assertions.assertEquals("Message sent", response);
        Assertions.assertTrue(database.isUserExists("mario"));
    }

    @Test
    void sendTooLongMessage() throws SQLException {
        server.addUser("mario", "bros");
        server.login("kamil", "pass");
        String response = server.sendMessage("mario", "TOOLOOOOONG!TOOLOOOOONG!TOOLOOOOONG!TOOLOOOOONG" + "!TOOLOOOOONG!TOOLOOOOONG!TOOLOOOOONG!TOOLOOOOONG!TOOLOOOOONG!TOOLOOOOONG!TOOLOOOOONG!TOOLOOOOONG!" + "TOOLOOOOONG!TOOLOOOOONG!TOOLOOOOONG!TOOLOOOOONG!TOOLOOOOONG!TOOLOOOOONG!" + "TOOLOOOOONG!TOOLOOOOONG!TOOLOOOOONG!TOOLOOOOONG!TOOLOOOOONG!TOOLOOOOONG!");

        Assertions.assertEquals("Message too long!", response);
    }

    @Test
    void inboxFull() throws SQLException {
        server.addUser("mario", "bros");
        server.login("kamil", "pass");
        String response = "";
        for (int i = 0; i < 6; i++) {
            response = server.sendMessage("mario", "message:" + i);
        }

        Assertions.assertEquals("mario's inbox is full", response);
    }


    // ----------------------SHOW MESSAGES TESTS---------------------

    @Test
    void showNotEmptyInbox() throws SQLException {
        server.addUser("mario", "bros");
        server.login("kamil", "pass");
        server.sendMessage("mario", "ok");
        server.login("mario", "bros");
        String response = database.showMessages("mario");
        System.out.println(response);
        Assertions.assertEquals("From kamil: ok", response);
    }

    @Test
    void showEmptyInbox() throws SQLException {
        server.addUser("mario", "bros");
        server.login("kamil", "pass");
        String response = database.showMessages("kamil");

        Assertions.assertNull(response);
    }


    // ----------------------SHOW USERDATA TESTS---------------------

    @Test
    void showUserdataForNonAdmin() throws SQLException {
        server.addUser("mario", "bros");
        server.login("kamil", "pass");
        String response = server.showUserData();

        Assertions.assertEquals("Username: kamil    Password: pass     Messages: null  Admin: false", response);
    }

    @Test
    void showUserdataForAdmin() throws SQLException {
        server.addUser("mario", "bros");
        server.login("kamil", "pass");
        server.setAdmin("kamil", "admin");
        String response = server.showUserData();
        Assertions.assertEquals("[kamil|pass|1|null, mario|bros|0|null]", response);
    }

    @Test
    void checkIfRemovingUserUnsettingAdmin() throws SQLException {
        server.addUser("mario", "bros");
        server.login("kamil", "pass");
        server.setAdmin("kamil", "admin");
        server.removeUser("mario");
        Assertions.assertTrue(database.isUserAdmin("kamil"));

    }


}

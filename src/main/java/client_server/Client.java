package client_server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    Socket socket;
    Scanner scanner;
    PrintWriter out;
    BufferedReader in;

    public static void main(String[] args) throws IOException {
        Client client = new Client();
        client.sendRequest();
    }

    public void sendRequest() throws IOException {
        socket = new Socket("localhost", 4999);
        scanner = new Scanner(System.in);
        out = new PrintWriter(socket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        System.out.println(in.readLine());
        String command;
        while ((command = scanner.nextLine()) != null) {
            out.println(command);
            getResponse();
        }
    }

    private void getResponse() throws IOException {
        String response = in.readLine();
        if (response == null) {
            System.out.println("Server stop working!");
        } else {
            System.out.println("Server: " + response);
        }
    }
}

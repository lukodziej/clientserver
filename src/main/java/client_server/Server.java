package client_server;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Server {

    final LocalDateTime serverStartStamp = LocalDateTime.now();
    final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    private final String createDate = serverStartStamp.format(dateTimeFormatter);

    private final Database database = new Database();
    private String returnMessage;
    private ServerSocket serverSocket;
    private PrintWriter out;
    private BufferedReader in;
    private String loggedUser = null;


    public static void main(String[] args) throws Exception {
        Server server = new Server();
        server.sendResponse();
    }

    private void sendResponse() throws Exception {
        serverSocket = new ServerSocket(4999);
        Socket socket = serverSocket.accept();
        final String version = "0.2.0";
        System.out.println("Client Connected");
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(socket.getOutputStream(), true);
        printer("Welcome on server ! Type your action: ");
        String command;
        while ((command = in.readLine()) != null) {
            System.out.println("Client:" + command);
            switch (command) {
                case "uptime" -> printer(uptime());
                case "info" -> printer("Create Date:" + createDate + ", Server Version: " + version);
                case "help" ->
                        printer("available commands: uptime - server live time, info - version and create date, stop - stop server, add - add user, login - log user, send - send message, show - show inbox, admin - set admin, data - show user/users data");
                case "stop" -> stop();
                case "" -> printer("Empty command");
                case "add" -> {
                    printer("Creating new user:");
                    String name = in.readLine();
                    printer("Type password");
                    String password = in.readLine();
                    printer(addUser(name, password));
                }
                case "login" -> {
                    printer("Type your username: ");
                    String username = in.readLine();
                    printer("Type your password: ");
                    String password = in.readLine();
                    if (login(username, password)) {
                        printer(username + " logged successfully");
                    } else {
                        printer("Invalid user data!");
                    }
                }
                case "delete" -> {
                    if (checkLogin()) break;
                    printer("Type nickname: ");
                    String username = in.readLine();
                    printer(removeUser(username));
                }
                case "send" -> {
                    if (checkLogin()) break;
                    printer("Send message to: ");
                    String username = in.readLine();
                    printer("Message body: ");
                    String message = in.readLine();
                    printer(sendMessage(username, message));
                }
                case "show" -> {
                    if (checkLogin()) break;
                    showMessages();
                }
                case "admin" -> {
                    if (checkLogin()) break;
                    printer("Which user privileges should be changed: ");
                    String username = in.readLine();
                    printer("Type admin password: ");
                    String password = in.readLine();
                    printer(setAdmin(username, password));
                }
                case "data" -> {
                    if (checkLogin()) break;
                    printer(showUserData());
                }
                default -> printer("Invalid command");
            }
        }
    }

    public String uptime() {
        LocalDateTime timeNow = LocalDateTime.now();
        Duration duration = Duration.between(serverStartStamp, timeNow);
        return "Server is running for: " + durationFormat(duration);
    }

    private void stop() throws IOException {
        serverSocket.close();
        in.close();
        out.close();
        System.exit(0);
    }

    public String addUser(String username, String password) throws SQLException {
        database.addUser(username, password);
        return "User: " + username + " created.";
    }

    public String removeUser(String username) throws SQLException {
        if (!database.isUserAdmin(loggedUser)) {
            return "You are not Admin!";
        }
        if (!database.isUserExists((username))) {
            return "User doesn't exists!";
        }
        database.removeUser(username);
        return "User removed";
    }

    public String setAdmin(String username, String password) throws SQLException {
        String adminPass = "admin";
        if (database.isUserExists(username) && adminPass.equals(password)) {
            database.setAdmin(username);
            returnMessage = "User: " + username + " privileges upgraded to admin";
        }
        if (!adminPass.equals(password)) {
            return "Wrong admin password";
        }
        if (!database.isUserExists(username)) {
            return "User doesn't exists!";
        }
        return returnMessage;
    }

    public String sendMessage(String username, String message) throws SQLException {
        if (database.isUserExists(username)) {
            if (database.messageCounter(username) >= 4) {
                return username + "'s" + " inbox is full";
            } else {
                if (message.length() > 255) {
                    return "Message too long!";
                } else {
                    database.sendMessage(username, "From " + loggedUser + ": " + message);
                    returnMessage = ("Message sent");
                }
            }
        }
        if (!database.isUserExists(username)) {
            return "Wrong user!";
        }
        return returnMessage;
    }

    public boolean login(String username, String password) throws SQLException {
        loggedUser = null;
        boolean logged = false;
        if (database.isUserPassValid(username, password)) {
            loggedUser = username;
            logged = true;
        }
        return logged;
    }

    public void showMessages() throws SQLException {
        printer(database.showMessages(loggedUser));
    }

    public String showUserData() throws SQLException {
        if (database.isUserExists(loggedUser) && !database.isUserAdmin(loggedUser)) {
            returnMessage = database.showData(loggedUser);
        }
        if (database.isUserAdmin(loggedUser)) {
            returnMessage = database.showDataAdmin();
        }
        return returnMessage;
    }

    public boolean checkLogin() {
        if (loggedUser == null) {
            printer("No logged user!");
            return true;
        }
        return false;
    }

    private String durationFormat(Duration duration) {
        return String.format("%d:%02d:%02d", duration.toHours(), duration.toMinutesPart(), duration.toSecondsPart());
    }

    private void printer(String message) {
        out.println(message);
    }

}
package client_server;

import java.sql.*;
import java.util.ArrayList;

public class Database {

    private static final String URL = "jdbc:sqlite:C:/sqlite/db/users.db";

    public void deleteAll() throws SQLException {
        Connection conn = DriverManager.getConnection(URL);
        PreparedStatement preparedStatement = conn.prepareStatement("delete from _data");
        preparedStatement.execute();
        conn.close();
    }

    public String showMessages(String userName) throws SQLException {
        Connection conn = DriverManager.getConnection(URL);
        PreparedStatement preparedStatement = conn.prepareStatement("select messages from _data where username= ?");
        preparedStatement.setString(1, userName);
        ResultSet resultSet = preparedStatement.executeQuery();
        String messages = resultSet.getString(1);
        conn.close();
        return messages;
    }

    public String showData(String userName) throws SQLException {
        Connection conn = DriverManager.getConnection(URL);
        PreparedStatement preparedStatement = conn.prepareStatement("select * from _data where username=?");
        preparedStatement.setString(1, userName);

        String name = String.valueOf(preparedStatement.executeQuery().getString("username"));
        String pass = String.valueOf(preparedStatement.executeQuery().getString("password"));
        String messages = String.valueOf(preparedStatement.executeQuery().getString("messages"));
        boolean admin = Boolean.parseBoolean(String.valueOf(preparedStatement.executeQuery().getString("admin")));
        conn.close();
        return "Username: " + name + "    Password: " + pass + "     Messages: " + messages + "  Admin: " + admin;
    }

    public String showDataAdmin() throws SQLException {
        Connection conn = DriverManager.getConnection(URL);
        ResultSet resultSet = conn.createStatement().executeQuery("select * from _data");
        ArrayList<String> arrayList = new ArrayList<>();
        while (resultSet.next()) {
            arrayList.add(resultSet.getString(1) + "|" +
                    resultSet.getString(2) + "|" +
                    resultSet.getString(3) + "|" +
                    resultSet.getString(4));
        }
        conn.close();
        return String.valueOf(arrayList);
    }

    public boolean isUserAdmin(String userName) throws SQLException {
        Connection conn = DriverManager.getConnection(URL);
        PreparedStatement preparedStatement = conn.prepareStatement("select admin from _data where username=?");
        preparedStatement.setString(1, userName);
        boolean isUserAdmin = preparedStatement.executeQuery().getString(1).equals("1");
        conn.close();
        return isUserAdmin;
    }

    public boolean isUserExists(String userName) throws SQLException {
        Connection conn = DriverManager.getConnection(URL);
        PreparedStatement preparedStatement = conn.prepareStatement("select count(*) from _data where username=?");
        preparedStatement.setString(1, userName);
        int count = Integer.parseInt(preparedStatement.executeQuery().getString(1));
        conn.close();
        return count != 0;
    }

    public boolean isUserPassValid(String userName, String pass) throws SQLException {
        Connection conn = DriverManager.getConnection(URL);
        PreparedStatement preparedStatement = conn.prepareStatement("select count(*) from _data where username=? and password=?");
        preparedStatement.setString(1, userName);
        preparedStatement.setString(2, pass);
        int count = Integer.parseInt(String.valueOf(preparedStatement.executeQuery().getString(1)));
        conn.close();
        return count != 0;
    }

    public void setAdmin(String userName) throws SQLException {
        Connection conn = DriverManager.getConnection(URL);
        PreparedStatement preparedStatement = conn.prepareStatement("update _data set admin=1 where username=?");
        preparedStatement.setString(1, userName);
        preparedStatement.executeUpdate();
        conn.close();
    }

    public void addUser(String userName, String pass) throws SQLException {
        Connection conn = DriverManager.getConnection(URL);
        if (userName.equals("") || pass.equals("")) return;
        PreparedStatement preparedStatement = conn.prepareStatement("insert into _data(username,password,admin) values (?,?,?)");
        preparedStatement.setString(1, userName);
        preparedStatement.setString(2, pass);
        preparedStatement.setInt(3, 0);
        preparedStatement.executeUpdate();
        conn.close();
    }

    public void removeUser(String userName) throws SQLException {
        Connection conn = DriverManager.getConnection(URL);
        PreparedStatement preparedStatement = conn.prepareStatement("delete from _data where username=?");
        preparedStatement.setString(1, userName);
        preparedStatement.executeUpdate();
        conn.close();
    }

    public void sendMessage(String userName, String message) throws SQLException {
        Connection conn = DriverManager.getConnection(URL);
        PreparedStatement preparedStatement = conn.prepareStatement("select messages from _data where username=?");
        preparedStatement.setString(1, userName);
        String messages = preparedStatement.executeQuery().getString(1);
        PreparedStatement preparedStatement2 = conn.prepareStatement("update _data set messages=? where username=?");
        if (messages == null) {
            preparedStatement2.setString(1, message);
            preparedStatement2.setString(2, userName);
            preparedStatement2.executeUpdate();
        }
        if (messages != null) {
            preparedStatement2.setString(1, messages + " | " + message);
            preparedStatement2.setString(2, userName);
            preparedStatement2.executeUpdate();
        }
        conn.close();
    }

    public long messageCounter(String userName) throws SQLException {
        Connection conn = DriverManager.getConnection(URL);
        PreparedStatement preparedStatement = conn.prepareStatement("select messages from _data where username=?");
        preparedStatement.setString(1, userName);
        String messages = String.valueOf(preparedStatement.executeQuery().getString("messages"));
        conn.close();
        return messages.chars().filter(ch -> ch == '|').count();
    }

}

